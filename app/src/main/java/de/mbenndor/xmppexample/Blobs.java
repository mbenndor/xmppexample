package de.mbenndor.xmppexample;/*
 * Created by m on 26.08.15.
 */

import android.util.Log;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.android.AndroidSmackInitializer;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

public class Blobs {
    public void blubs(){
        try {
            new AndroidSmackInitializer().initialize();
            //SmackConfiguration.setDefaultPacketReplyTimeout(15000);
            SmackConfiguration.DEBUG=true;
            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                    .setServiceName("jabber.ccc.de")
                    .setHost("jabber.ccc.de")
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                            //.setPort(5222)
                    .build();


            AbstractXMPPConnection conn1 = new XMPPTCPConnection(config);
            conn1.connect();

            conn1.login("REPLACEME", "REPLACEME");
            // Assume we've created an XMPPConnection name "connection"._
            ChatManager chatmanager = ChatManager.getInstanceFor(conn1);
            org.jivesoftware.smack.chat.Chat chat = chatmanager.createChat("REPLACEME@REPLACEME", new ChatMessageListener() {
                @Override
                public void processMessage(org.jivesoftware.smack.chat.Chat chat, org.jivesoftware.smack.packet.Message message) {
                    System.out.println("Received message: " + message);
                }
            });
            chat.sendMessage("Howdy");
        }
        catch (Exception ex){
            Log.e("MAIN", ex.getMessage());
        }
    }
}